<?php

/**
 * @file
 * Functions to support theming in the classic admin theme theme.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function classic_admin_theme_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  // Add a textfield for a background color setting.
  $form['classic_admin_theme_settings']['background'] = [
    '#type' => 'fieldset',
    '#title' => t('Classic Admin Theme Color Scheme Settings'),
    '#attributes' => [
      'class' => ['claro-details'],
    ],
  ];

  $form['classic_admin_theme_settings']['background']['description'] = [
    '#type' => 'html_tag',
    '#tag' => 'p',
    '#value' => t('These settings adjust the look and feel of the Classic Admin Theme theme. Changing the color below will change the base hue, saturation, and lightness values the Classic Admin Theme theme uses to determine its internal colors.'),
  ];

  $form['classic_admin_theme_settings']['background']['background_color'] = [
    '#type' => 'select',
    '#title' => t('Background Color Options'),
    '#default_value' => theme_get_setting('bg_color'),
    '#options' => [
      '#FFFFFF' => t('Default (White)'),
      '#000000' => t('Black'),
      '#FC4445' => t('Red'),
      '#5CDB95' => t('Green'),
      '#98AFC7' => t('Blue Gray'),
      '#8C9EC7' => t('Metallic Blue Gray'),
      '#FAF5E9' => t('Ivory'),
      '#31EC56' => t('Malachite Green'),
      '#EE72F8' => t('Heliotrope'),
      '#F0E68C' => t('Lilac'),
      '#DDD0C8' => t('Beige'),
      '#C5ADC5' => t('Pastel Purple'),
      '#B2B5E0' => t('Light Steel Blue'),
      '#F0E68C' => t('Lilac'),
    ],
    '#attributes' => [
      'name' => 'slected_background',
    ],
  ];

  $form['classic_admin_theme_settings']['background']['bg_color'] = [
    '#type' => 'textfield',
    '#default_value' => theme_get_setting('bg_color'),
    '#attributes' => [
      'id' => 'bg_color_hex',
    ],
  ];

  $form['classic_admin_theme_settings']['background']['bg_color_hex'] = [
    '#title' => t('Background Color'),
    '#description' => t('Background color for icon wrapper.'),
    '#type' => 'color',
    '#default_value' => theme_get_setting('bg_color'),
    '#disabled' => TRUE,
    '#attributes' => [
      'id' => 'bg_color',
    ],
  ];

  $form['classic_admin_theme_settings']['background']['select_text_color'] = [
    '#type' => 'select',
    '#title' => t('Text Color Options'),
    '#default_value' => theme_get_setting('text_color_hex'),
    '#options' => [
      '#000000' => t('Default (Black)'),
      '#FFFFFF' => t('White'),
      '#FF0000' => t('Red'),
      '#009B4D' => t('Green'),
      '#0000FF' => t('Blue'),
      '#FFFF00' => t('Yellow'),
      '#808080' => t('Gray'),
    ],
    '#attributes' => [
      'name' => 'selected_text_color',
      'id' => 'selected_text_color',
    ],
  ];

  $form['classic_admin_theme_settings']['background']['text_color_hex'] = [
    '#type' => 'textfield',
    '#default_value' => theme_get_setting('text_color_hex'),
    '#attributes' => [
      'id' => 'text_color_hex',
    ],
  ];
}
