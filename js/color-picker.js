/**
 * @file
 * Media Library overrides for Classic Admin Theme
 */
((Drupal, window) => {
  /**
   * Update the field option based on the selected color.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behavior to select media items.
   */
  Drupal.behaviors.SelectColorForClassicAdminTheme = {
    attach() {
      // Background Color selection
      const backgroundOptions = document.querySelector(
        '[data-drupal-selector="edit-background-color"]'
      );
      const backgroundColor = document.getElementById("bg_color");
      const backgroundColorHex = document.getElementById("bg_color_hex");

      let textColorOption = document.getElementById("selected_text_color");
      const textColorHex = document.getElementById("text_color_hex");
      if (backgroundOptions) {
        alert('asasd');
        backgroundOptions.addEventListener("change", (e) => {
          backgroundColor.value = e.target.value;
          backgroundColorHex.value = e.target.value;

          // Disable the text color option which is selected for background

          const selectedOption =
            backgroundOptions.options[backgroundOptions.selectedIndex].text;
          for (var i = 0; i < textColorOption.options.length; i++) {
              textColorOption.options[i].text === selectedOption ? textColorOption.options[i].disabled = TRUE : textColorOption.options[i].disabled = FALSE;
          }
        });
      }

      if (backgroundOptions) {
        backgroundColor.addEventListener("change", (e) => {
          backgroundColorHex.value = e.target.value;
        });
      }

      // Text Color selection
      if (textColorOption) {
        textColorOption.addEventListener("change", (e) => {
          textColorHex.value = e.target.value;
        });
      }
    },
  };
})(Drupal, window);
