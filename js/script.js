(function ($) {
	Drupal.behaviors.classic_admin_theme = {
		attach: function (context, settings) {
			$( ".menuIcon", context).once().on( "click", function() {
				if($('body').hasClass('slideMenu')) {
					$('body').removeClass('slideMenu');
				}else {
					$('body').addClass('slideMenu');
				}
			});
		}
	};

}(jQuery));